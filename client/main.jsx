import React from 'react'
import ReactDOM from 'react-dom'

const App = () => <div>Hello World!</div>

Meteor.startup(() => ReactDOM.render(<App />, document.querySelector('#app')))
